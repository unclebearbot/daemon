#!/bin/bash

app_home=`dirname ${0}`
java=${app_home}/jre/bin/javaw
[[ ! -x ${java} && -d ${JAVA_HOME} ]] && java=${JAVA_HOME}/bin/javaw
[[ ! -x ${java} ]] && java=javaw

cd "${app_home}" && nohup "${java}" -jar "${app_home}/lib/app.jar" &
