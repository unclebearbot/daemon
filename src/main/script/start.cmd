@echo off

set app_home=%~dp0%
set java=%app_home%\jre\bin\javaw.exe
if not exist "%java%" if exist %JAVA_HOME% set java=%JAVA_HOME%\bin\javaw.exe
if not exist "%java%" set java=javaw

start "" /D "%app_home%" "%java%" -jar "%app_home%\lib\app.jar"
