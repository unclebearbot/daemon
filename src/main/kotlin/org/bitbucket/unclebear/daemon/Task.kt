package org.bitbucket.unclebear.daemon

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException
import java.util.*

class Task @JsonCreator constructor(@JsonProperty("command") var command: String?, @JsonProperty("args") var args: String?, @JsonProperty("temp") var temp: String?, @JsonProperty("auto") var auto: Boolean? = false) {
    constructor() : this("", "", "")

    private val slash = "/"
    private val space = " "
    private val semicolon = ";"
    private val empty = ""
    private val interval = 1000L
    private val fileSeparatorProperty = "file.separator"
    private val separator = System.getProperty(fileSeparatorProperty) ?: slash
    private var process: Process? = null
    private val interrupted = "Interrupted: "
    private val starting = "Starting the process: "
    private val unableToStart = "Unable to start the process: "
    private val stopping = "Stopping the process: "
    private val stopped = "The process is stopped: "
    private val unableToStop = "Unable to stop the process: "
    private val restarting = "Restarting the process: "
    private val started = "Started for "
    private val time = " time: "
    private val times = " times: "
    private val removeTemp = "Removing temp: "
    private val unableToRemove = "Unable to remove "
    private val making = "Making: "
    private val unableToMake = "Unable to make "
    private val endingSeparators = "$separator+$"
    private val start = "Start"
    private val stop = "Stop"
    private var action = start
    private val remove = "Remove"
    @JsonIgnore var active = true
    @JsonIgnore val idProperty = SimpleStringProperty(UUID.randomUUID().toString())
    @JsonIgnore val commandProperty = SimpleStringProperty(command)
    @JsonIgnore val argsProperty = SimpleStringProperty(args)
    @JsonIgnore val tempProperty = SimpleStringProperty(temp)
    @JsonIgnore val autoProperty = SimpleObjectProperty<Boolean>(auto)
    @JsonIgnore val actionProperty = SimpleObjectProperty<String>(action)
    @JsonIgnore val removeProperty = SimpleObjectProperty<String>(remove)

    init {
        if (auto ?: false) start()
    }

    fun start() {
        if (validate()) {
            actionProperty.set(stop)
            Thread { launch(command!!, args!!.split(space), temp!!.split(semicolon) as MutableList<String>) }.start()
        }
    }

    fun stop() {
        actionProperty.set(start)
        active = false
        Log.info(stopping + this)
        try {
            process?.destroy()
            Log.info(stopped + this)
        } catch (exception: Exception) {
            Log.error(unableToStop + this)
        }
    }

    fun validate(): Boolean {
        if (command.isNullOrBlank()) return false
        if (args.isNullOrBlank()) args = empty
        if (temp.isNullOrBlank()) temp = empty
        return true
    }

    fun launch(executable: String, arguments: List<String>, temp: MutableList<String>): Boolean {
        var count = 0L
        val command = mutableListOf<String>()
        command.add(executable)
        command.addAll(arguments)
        temp.remove(empty)
        while (active) {
            if (count > 0) {
                try {
                    Thread.sleep(interval)
                } catch (exception: InterruptedException) {
                    Log.warn(interrupted + this)
                    stop()
                    Thread.currentThread().interrupt()
                }
            }
            if (process?.isAlive ?: false) continue
            if (!active) break
            if (count == 0L) {
                Log.info(starting + this)
            } else {
                Log.warn(stopped + this)
                clean(temp)
                Log.warn(restarting + this)
            }
            try {
                process = ProcessBuilder().command(command).start()
                Log.info(started + ++count + (if (count == 1L) time else times) + this)
            } catch (exception: Exception) {
                Log.error(unableToStart + this)
                break
            }
        }
        return false
    }

    fun clean(resources: List<String>) {
        for (temp in resources) {
            val file = File(temp)
            val make = temp.endsWith(separator) && file.isDirectory
            val path = qualify(temp)
            if (file.exists()) {
                Log.info(removeTemp + path)
                try {
                    FileUtils.forceDelete(file)
                } catch (exception: IOException) {
                    Log.warn(unableToRemove + path)
                    continue
                }
                if (make) {
                    Log.info(making + path)
                    try {
                        FileUtils.forceMkdir(file)
                    } catch (exception: IOException) {
                        Log.warn(unableToMake + path)
                    }
                }
            }
        }
    }

    fun qualify(path: String) = path.replace(Regex(endingSeparators), empty)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Task) return false
        if (command != other.command) return false
        if (args != other.args) return false
        if (temp != other.temp) return false
        if (auto != other.auto) return false
        return true
    }

    override fun hashCode(): Int {
        var result = command?.hashCode() ?: 0
        result = 31 * result + (args?.hashCode() ?: 0)
        result = 31 * result + (temp?.hashCode() ?: 0)
        result = 31 * result + (auto?.hashCode() ?: 0)
        return result
    }

    override fun toString() = "Task(command=$command, args=$args, temp=$temp, auto=$auto)"
}
