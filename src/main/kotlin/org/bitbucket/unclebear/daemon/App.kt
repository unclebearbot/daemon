package org.bitbucket.unclebear.daemon

import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import kotlin.system.exitProcess

class App : Application() {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) = App().launch()
    }

    val title = "Task Daemon"
    val icon = "App.png"
    override fun start(primaryStage: Stage) {
        primaryStage.title = title
        primaryStage.scene = Scene(ManagerController().load())
        primaryStage.icons.add(Image(this::class.java.getResourceAsStream(icon)))
        primaryStage.isResizable = false
        primaryStage.onCloseRequest = EventHandler {
            Manager.controller!!.save()
            exitProcess(0)
        }
        primaryStage.show()
    }

    fun launch() = Application.launch()
}
