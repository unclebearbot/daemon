package org.bitbucket.unclebear.daemon

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import java.net.URL
import java.util.*

class TaskController : HBox(), Initializable {
    val fxml = "Task.fxml"
    var task: Task? = null
    val commandPrompt = "Command"
    val argsPrompt = "Args"
    val tempPrompt = "Temp"
    val start = "Start"
    val stop = "Stop"
    @FXML
    var id: TextField? = null
    @FXML
    var command: TextField? = null
    @FXML
    var args: TextField? = null
    @FXML
    var temp: TextField? = null
    @FXML
    var auto: CheckBox? = null
    @FXML
    var action: Button? = null
    @FXML
    var remove: Button? = null

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        task = Manager.load()
        id!!.textProperty().bindBidirectional(task!!.idProperty)
        command!!.textProperty().bindBidirectional(task!!.commandProperty)
        args!!.textProperty().bindBidirectional(task!!.argsProperty)
        temp!!.textProperty().bindBidirectional(task!!.tempProperty)
        auto!!.selectedProperty().bindBidirectional(task!!.autoProperty)
        action!!.textProperty().bindBidirectional(task!!.actionProperty)
        remove!!.textProperty().bindBidirectional(task!!.removeProperty)
    }

    fun action() = if (action!!.text == start) start() else stop()
    fun start() = task!!.start()
    fun stop() = task!!.stop()
    fun removeFromManager() = Manager.controller!!.remove(this)
    fun load() = FXMLLoader.load<Parent>(this::class.java.getResource(fxml))!!
}
