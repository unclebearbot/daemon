package org.bitbucket.unclebear.daemon

import org.apache.commons.io.FileUtils
import java.io.File
import java.io.FileOutputStream
import java.io.PrintStream
import java.text.SimpleDateFormat
import java.util.*

class Log private constructor() {
    companion object {
        var file = "var/log.txt"
        var stream: PrintStream? = null
        val info = "[INFO] "
        val warn = "[WARN] "
        val error = "[ERROR]"
        val format = "yyyy-MM-dd HH:mm:ss z"
        fun info(message: String) = log(info, message)
        fun warn(message: String) = log(warn, message)
        fun error(message: String) = log(error, message)
        private fun log(level: String, message: String) {
            try {
                val log = File(file)
                if (!log.exists()) FileUtils.touch(log)
                if (stream == null) stream = PrintStream(FileOutputStream(log))
                stream?.println("$level ${SimpleDateFormat(format).format(Date())} - $message")
            } catch (ignore: Exception) {
            }
        }
    }
}
