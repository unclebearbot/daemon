package org.bitbucket.unclebear.daemon

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.control.CheckBox
import javafx.scene.control.SplitPane
import javafx.scene.control.TextField
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import java.net.URL
import java.util.*

class ManagerController : SplitPane(), Initializable {
    val fxml = "Manager.fxml"
    val empty = ""
    val commandPrompt = "Command"
    val argsPrompt = "Args"
    val tempPrompt = "Temp"
    val start = "Start"
    @FXML
    var flowPane: FlowPane? = null

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        Manager.controller = this
        init()
    }

    fun init() {
        while (Manager.index < Manager.tasks.size) {
            flowPane!!.children.add(TaskController().load())
        }
    }

    fun newTask() {
        Manager.add(Task())
        flowPane!!.children.add(TaskController().load())
    }

    fun save() {
        Manager.clear()
        flowPane!!.children.forEach { node ->
            node as HBox
            val task = Task()
            node.children.forEach { child ->
                if (child is TextField) {
                    if (child.promptText == commandPrompt) task.command = child.text
                    else if (child.promptText == argsPrompt) task.args = child.text
                    else if (child.promptText == tempPrompt) task.temp = child.text
                } else if (child is CheckBox) {
                    task.auto = child.isSelected
                }
            }
            Manager.add(task)
        }
        Manager.save()
    }

    fun remove(taskController: TaskController) {
        flowPane!!.children.forEach { node ->
            node as HBox
            node.children.forEach { child ->
                if (child is TextField && !child.isVisible && child.text == taskController.id!!.text) {
                    flowPane!!.children.remove(node)
                    return
                }
            }
        }
    }

    fun load() = FXMLLoader.load<Parent>(this::class.java.getResource(fxml))!!
}
