package org.bitbucket.unclebear.daemon

import com.fasterxml.jackson.databind.ObjectMapper
import javafx.collections.FXCollections
import org.apache.commons.io.FileUtils
import java.io.File
import java.util.*

class Manager private constructor() {
    companion object {
        var controller: ManagerController? = null
        var file = "data/tasks.js"
        val tasks = FXCollections.observableArrayList<Task>()!!
        var index = 0
        val loading = "Loading: "
        val added = "Added: "
        val loaded = "Loaded: "
        val unableToLoad = "Unable to load: "
        val saved = "Saved: "
        val unableToSave = "Unable to save: "
        val made = "Made: "

        init {
            val file = File(file)
            try {
                Log.info(loading + file.absolutePath)
                read(file).forEach(this::add)
                Log.info(loaded + file.absolutePath)
            } catch (exception: Exception) {
                Log.warn(unableToLoad + file.absolutePath)
            }
        }

        fun add(task: Task) {
            tasks.add(task)
            Log.info(added + task.toString())
        }

        fun clear() {
            index = 0
            tasks.clear()
        }

        fun load(): Task {
            return tasks[index++]
        }

        fun save() {
            val file = File(file)
            try {
                if (!file.exists()) {
                    FileUtils.touch(file)
                    Log.info(made + file.absolutePath)
                }
                write(file)
                Log.info(saved + file.absolutePath)
            } catch (exception: Exception) {
                Log.warn(unableToSave + file.absolutePath)
            }
        }

        private fun read(file: File): ArrayList<Task> {
            val mapper = ObjectMapper()
            return mapper.readValue(file, mapper.typeFactory.constructCollectionType(ArrayList::class.java, Task::class.java))
        }

        private fun write(file: File) = ObjectMapper().writeValue(file, tasks)
    }
}
